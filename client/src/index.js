import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { LoadScript } from '@react-google-maps/api';
import { RecoilRoot } from 'recoil';

import "assets/plugins/nucleo/css/nucleo.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "assets/scss/argon-dashboard-react.scss";

import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";

import AdminLayout from "layouts/Admin.js";
import AuthLayout from "layouts/Auth.js";
import PassReset from "layouts/Auth_PassReset.js";
import TermsConditionsLayout from "layouts/TermsConditions.js";
import ChatLayout from "layouts/Chat.js";
import ProfileLayout from "layouts/Profile.js";
import RoutesLayout from  "layouts/Routes.js";

ReactDOM.render(
  <LoadScript
    googleMapsApiKey={process.env.REACT_APP_GOOGLE_API_KEY}
    libraries={['drawing']}
  >
    <RecoilRoot>
      <I18nextProvider i18n={i18n}>
        <BrowserRouter>
          <Switch>
            <Route path="/auth" render={props => <AuthLayout {...props} />} />
            <Route path="/admin" render={props => <AdminLayout {...props} />} />
            <Route path="/profile" render={props => <ProfileLayout {...props} />} />
            <Route path="/route" render={props => <RoutesLayout {...props} />} />
            <Route path="/reset" render={props => <PassReset {...props} />} />
            <Route path="/info" render={props => <TermsConditionsLayout {...props} />} />
            <Route path="/chat" render={props => <ChatLayout {...props} />} />
            <Redirect exact to="/auth/login" />
          </Switch>
        </BrowserRouter>
      </I18nextProvider>
    </RecoilRoot>
  </LoadScript>,
  document.getElementById("root")
);
