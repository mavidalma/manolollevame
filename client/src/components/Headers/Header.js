import React from 'react';
import T from 'prop-types';
// reactstrap components
import { Container, Col } from 'reactstrap';
import SearchMap from '../Maps/SearchMap';
import { withTranslation } from 'react-i18next';

function Header({ fetchRoutes, ...props }) {
  const { t } = props;
  return (
    // <>
    //   <SearchRoutes fetchRoutes={fetchRoutes} />
    // </>

<>
<div
  className="header pb-8 pt-0 pt-sm-5 pt-lg-8 d-flex align-items-center"
  style={{
    minHeight: '600px',
    backgroundImage: 'url(' + require('assets/img/theme/voyaqui_gmaps.jpg') + ')',
    //backgroundSize: 'cover',
    backgroundPosition: 'center top',
  }}
>
  {/* Mask */}
  <span className="mask bg-gradient-default opacity-8" />
  {/* Header container */}
  <Container className="d-flex align-items-center p-0" fluid>

      <Col className="p-0">
      <SearchMap fetchRoutes={fetchRoutes} />
      </Col>
 
  </Container>
</div>
</>
  );
}

Header.propTypes = {
  fetchRoutes: T.func.isRequired,
};

export default withTranslation('translations')(Header);
