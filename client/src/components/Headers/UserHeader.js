import React from 'react';
import { withTranslation } from 'react-i18next';

// reactstrap components
import { Container, Row, Col } from 'reactstrap';

function UserHeader({ name, owner, ...props }) {
  const { t } = props;

  return (
    <>
      <div
        className="header pb-8 pt-5 pt-lg-8 d-flex align-items-center"
        style={{
          minHeight: '600px',
          backgroundImage: 'url(' + require('assets/img/theme/profile-cover.jpg') + ')',
          backgroundSize: 'cover',
          backgroundPosition: 'center top',
        }}
      >
        {/* Mask */}
        <span className="mask bg-gradient-default opacity-8" />
        {/* Header container */}
        <Container className="d-flex align-items-center" fluid>
          <Row>
            <Col md="10">
              {owner ? (
                <div>
                  <h1 className="display-2 text-white">
                    {t('UserHeader.hello')} {name ? name : 'user'}
                  </h1>
                  <p className="text-white mt-0 mb-5">{t('UserHeader.message')}</p>
                </div>
              ) : (
                <div>
                  <h1 className="display-2 text-white">
                    {t('UserHeader.user')} {name ? name : 'an user'}
                  </h1>
                  <p className="text-white mt-0 mb-5">{t('UserHeader.user_message')}</p>
                </div>
              )}
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default withTranslation('translations')(UserHeader);
