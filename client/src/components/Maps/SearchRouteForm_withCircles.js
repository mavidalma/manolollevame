import React, { useState } from "react";
import { Row, Col, FormGroup, Form, Label, Input, Button } from "reactstrap";

import { Circle, GoogleMap, DrawingManager, Data } from '@react-google-maps/api';
import { usePosition } from '../../helpers/usePosition';

import "./styles.css";

function SearchRoutes(){

    const {userLat, userLng, error} = usePosition();
    const [ id, setId ] = useState(0);
    const [ markers, setMarkers ] = useState([]);
    const [distanceMargin, setDistanceMargin] = useState(1000);
    const [radius, setRadius] = useState();
    const [points, setPoints] = useState([]);


    const addMarker = (coords, radius) => {
      setId((id)=>id+1);
      setMarkers((markers) => [...markers, {id, coords, radius}] )
    }

    const addPoint = (lat, lng) => {
      setPoints((points) => [...points, {lat, lng}] )
    }
 
    const handleChange = e => {
        e.preventDefault();
        const value = e.target.value;
        const name = e.target.name;
        console.log(markers)
      };
    
    const handleSubmit = async (e) => {
        e.preventDefault();
        //do something with route
       // setRoute({startingPoint: markers[0], arrivalPoint: markers[1], schedule:""})
        console.log("route: ", markers)
    };

    const updateMarker = (markerId, e) => {
        markers.map(marker => marker.id === markerId ? marker.coords = e.latLng.toJSON() : null )
        console.log(markers)
    }

    const updateRadius = (markerId, e) => {
        
        //markers.map(marker => marker.id === markerId ? marker.radius = e.latLng.toJSON() : null )
        console.log(markers)
    }

    const deleteMarker = (markerId, e) => {
        console.log(markerId);
        console.log(e)
        //markers.map(marker => marker.id === markerId ? e.target.setMap(null): null)
        const filtered = markers.filter(marker => marker.id !== markerId);
        console.log(markers, "filtered: ", filtered)
        setMarkers(filtered);
    }

    const circleOptions = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        clickable: true,
        draggable: true,
        visible: true,
        zIndex: 1
      }
    
    const handleRadiusChange = (radius) => {
       // markers.map(marker => marker.id === id ? marker.radius = radius : null )
        
        console.log("hola", id, radius/*, "this radius: ", this.getradius()*/)
    }

    const drawingOptions = {
            drawingControlOptions: {
              drawingModes: [/*"marker", "circle", */"polygon"],
            },
            circleOptions: {
                fillColor: "#ffff00",
                fillOpacity: 1,
                strokeWeight: 5,
                clickable: true,
                editable: true,
                draggable: true,
                zIndex: 1,
                center_changed: ()=>console.log("center changed") 
              },
            markerOptions: {
                fillColor: "#ffffff",
                fillOpacity: 0.5,
                strokeWeight: 5,
                clickable: true,
                editable: true,
                draggable: true,
                zIndex: 1,
              },
            polygonOptions: {
              fillColor: "#ffffff",
              fillOpacity: 0.5,
              strokeWeight: 5,
              clickable: true,
              editable: true,
              zIndex: 1,
            }              
      }
  
      const drawingOnLoad = drawingManager => {
        console.log(drawingManager)
        drawingManager.addListener('click', ()=> {
          console.log("on drawing manager mouse over!")
        })
      }
      
      const onCircleComplete = circle => {
        console.log(circle, circle.center.lat(), circle.center.lng(), circle.radius);
        const position = {lat:circle.center.lat(), lng:circle.center.lng()}
        
        setId((id)=>id+1);
        circle.id= id;
        setRadius(circle.radius);
        console.log("radius on state: ", radius)
        const circleRadius = circle.getRadius();
        
        setMarkers((markers) => [...markers, {id, position, radius:circle.radius}]);
        
        circle.radius_changed= () => circle.getRadius();
        circle.onClick=()=>console.log("click!")
    
    };

      const onPolygonComplete = (polygon, destroy = false) => {

        console.log(polygon)
        
        polygon.onClick = (e) => console.log(e)

        for (let i = 0; i < polygon.getPath().getLength(); i++) {
        addPoint(polygon.getPath().getAt(i).lat(), polygon.getPath().getAt(i).lng()); 
        }

        if(destroy) {
          polygon.setMap(null);
        }

    };


      const onMarkerComplete = marker => {
        console.log(marker, marker.position.lat());
        marker.onClick = (e)=> console.log("click!");
        marker.draggable= true;
      }

    return(
<>
        <Row>
        <Col>
           {console.log("points: ", points)}
           <div className="dashboard-mapWrapper">
            <GoogleMap 
                mapContainerStyle={{width: '100%', height: `100%`}}
                center={{lat: userLat || 38.1834068, lng: userLng ||-3.69173 }}
                onClick={(e)=> addMarker(e.latLng.toJSON(), distanceMargin)}
                zoom={12}
            >
                {markers ? (
                markers.map((marker) => {
                return (
                    <Circle
                    key={marker.id}
                    draggable={true}
                    editable={true}
                    center={marker.coords}
                    radius={marker.radius}
                    options={circleOptions}
                    onDragEnd={e => updateMarker(marker.id, e)/*marker.coords = e.latLng.toJSON()*/}
                    onClick={e => deleteMarker(marker.id, e)}
                   // onRadiusChanged={e => updateRadius(marker.id, e)}
                    /> )
                })) : null }

                <DrawingManager
                    onLoad={drawingOnLoad}
                    options={drawingOptions}
                    drawingMode= {"polygon"}
                    onPolygonComplete={(polygon) => onPolygonComplete(polygon, false)}
                    onCircleComplete={onCircleComplete}
                    onMarkerComplete= {onMarkerComplete}
                    />
                
            </GoogleMap>
           </div> 
           <Form role="form" onSubmit={handleSubmit}>
                <FormGroup>
                    <Label for  = "startingPoint">I am leaving from</Label>
                    <Input type = "text" name = "startingPoint" id = "startingPoint" onChange={handleChange}/>
                </FormGroup>
                <FormGroup>
                    <Label for= "examparrivalPointleState">and arriving to</Label>
                    <Input type= "text" name = "arrivalPoint" id = "arrivalPoint" onChange={handleChange}/>
                </FormGroup>
                <FormGroup>
                    <Label for  = "distance">distance margin for match (in meters)</Label>
                    <Input type = "number" name = "disstance" id = "schedule" value={distanceMargin} onChange={e => {e.preventDefault(); setDistanceMargin(parseInt(e.target.value))}}/>
                </FormGroup>  
                <Button type="submit">Buscar</Button>
            </Form>  
            
            </Col>
            </Row>
        <Row>
            
    
        </Row>
        </>
    )
}

export default SearchRoutes;