/*eslint-disable*/
import React, { useState, useEffect } from 'react';
import { NavLink as NavLinkRRD, Link } from 'react-router-dom';
import { useRecoilState, useRecoilValue } from 'recoil';
import { userData } from '../../services/Atoms';
import Cookie from 'js-cookie';
import { withTranslation } from 'react-i18next';
import API from '../../services/API';

// nodejs library to set properties for components
import { PropTypes } from 'prop-types';

import config from '../../config';
import logOut from '../utils/LogOut';

// reactstrap components
import {
  Collapse,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Media,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
} from 'reactstrap';

var ps;

function Sidebar({ i18n, ...props }) {

  const [collapse, setCollapse] = useState(false);
  const [stateI18n, setStateI18n] = useState({ value: Cookie.get('manolo.locale') || 'en' });
  const [user, setUser] = useRecoilState(userData);
  const { t } = props;

  useEffect(() => {
    const logged = Cookie.get(config.cookieName);
    const userId = localStorage.getItem('user id');
    if (logged && userId) {
      (async () => {
        const user = await API.getUserById(userId);
        setUser(user)
      })()
    };

    // logged && userId ? setUser(userId) : null;
  }, []);

  const handleLogout = (evt) => {
    //evt.preventDefault();
    localStorage.clear();
    Cookie.remove(config.cookieName);
  };

  // verifies if routeName is the one active (in browser input)
  const activeRoute = (routeName) => {
    return props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
  };
  // toggles collapse between opened and closed (true/false)
  const toggleCollapse = () => {
    setCollapse(!collapse);
  };
  // closes the collapse
  const closeCollapse = () => {
    setCollapse(false);
  };

  //logs out user

  const logOutUser = () => {
    logOut();
    setUser(false);
    setCollapse(false);
  };
  // creates the links that appear in the left menu / Sidebar
  const createLinks = (routes) => {
    return routes.map((prop, key) => {
      if (prop.invisible) return null;
      if (prop.layout === '/reset' || prop.layout === '/info' || prop.layout === '/chat')
        return null;
      if (prop.available === 'any') {
        return (
          <NavItem key={key}>
            <NavLink
              to={prop.layout + prop.path}
              tag={NavLinkRRD}
              onClick={closeCollapse}
              activeClassName="active"
            >
              <i className={prop.icon} />
              {t(`Sidebar.${prop.name}`)}
            </NavLink>
          </NavItem>
        );
      } else if (user && prop.available === 'logged') {
        return (
          <NavItem key={key}>
            <NavLink
              to={prop.layout + prop.path}
              tag={NavLinkRRD}
              onClick={closeCollapse}
              activeClassName="active"
            >
              <i className={prop.icon} />
              {t(`Sidebar.${prop.name}`)}
            </NavLink>
          </NavItem>
        );
      } else if (!user && prop.available === 'public') {
        return (
          <NavItem key={key}>
            <NavLink
              to={prop.layout + prop.path}
              tag={NavLinkRRD}
              onClick={closeCollapse}
              activeClassName="active"
            >
              <i className={prop.icon} />
              {t(`Sidebar.${prop.name}`)}
            </NavLink>
          </NavItem>
        );
      }
    });
  };
  const { bgColor, routes, logo } = props;
  let navbarBrandProps;

  if (logo && logo.innerLink) {
    navbarBrandProps = {
      to: logo.innerLink,
      tag: Link,
    };
  } else if (logo && logo.outterLink) {
    navbarBrandProps = {
      href: logo.outterLink,
      target: '_blank',
    };
  }

  const handleChange = (event) => {
    const lang = event.target.alt;

    setStateI18n({ value: lang });
    Cookie.set('manolo-locale', lang);
    i18n.changeLanguage(lang);
  };

  return (
    <Navbar
      className="navbar-vertical fixed-left navbar-light bg-white"
      expand="lg"
      id="sidenav-main"
    >
      <Container fluid>
        <div>
          {/* Toggler */}
          <button className="navbar-toggler" type="button" onClick={toggleCollapse}>
            <span className="navbar-toggler-icon" />
          </button>
          {/* Brand */}
          {logo ? (
            <NavbarBrand className="pt-0" {...navbarBrandProps}>
              <img alt={logo.imgAlt} className="navbar-brand-img" src={logo.imgSrc} />
            </NavbarBrand>
          ) : null}
        </div>
        {/* User */}
        <Nav className="align-items-center d-lg-none">
          <UncontrolledDropdown nav>
            <DropdownToggle nav>
              <Media className="align-items-center">
                <span className="avatar avatar-sm rounded-circle">
                  <img alt="..." src={require('assets/img/theme/undraw_female_avatar.svg')} />
                </span>
              </Media>
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-arrow" right>
              <DropdownItem className="noti-title" header tag="div">
                <h6 className="text-overflow m-0">{t('AdminNavbar.dropdown_menu-welcome')} </h6>
              </DropdownItem>
              <DropdownItem to="/admin/index" tag={Link} className={user ? 'show' : 'd-none'}>
                <i className="ni ni-tv-2 text-info" />
                <span>{t('AdminNavbar.dropdown_menu-dashboard')} </span>
              </DropdownItem>
              <DropdownItem to="/profile/user" tag={Link} className={user ? 'show' : 'd-none'}>
                <i className="ni ni-single-02 text-info" />
                <span>{t('AdminNavbar.dropdown_menu-my-profile')} </span>
              </DropdownItem>
              <DropdownItem
                href={process.env.REACT_APP_API_URL}
                onClick={handleLogout}
                className={user ? 'show' : 'd-none'}
              >
                <i className="ni ni-user-run text-info" />
                <span>{t('AdminNavbar.dropdown_menu-logout')} </span>
              </DropdownItem>
              <DropdownItem to="/auth/login" tag={Link} className={!user ? 'show' : 'd-none'}>
                <i className="ni ni-key-25 text-info" />
                <span>{t('AdminNavbar.dropdown_menu-login')} </span>
              </DropdownItem>
              <DropdownItem to="/auth/register" tag={Link} className={!user ? 'show' : 'd-none'}>
                <i className="ni ni-circle-08 text-info" />
                <span>{t('AdminNavbar.dropdown_menu-register')} </span>
              </DropdownItem>
              <DropdownItem>
                <Link to="#" className="mr-2" onClick={handleChange}>
                  <img src={require('assets/img/lang/flag_UK.svg')} alt="en" className="flag-img" />
                </Link>
                <Link to="#" className="mr-2" onClick={handleChange}>
                  <img src={require('assets/img/lang/flag_ES.svg')} alt="es" className="flag-img" />
                </Link>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/* Collapse */}
        <Collapse navbar isOpen={collapse}>
          {/* Collapse header */}
          <div className="navbar-collapse-header d-md-none">
            <Row>
              {logo ? (
                <Col className="collapse-brand" xs="6">
                  {logo.innerLink ? (
                    <Link to={logo.innerLink}>
                      <img alt={logo.imgAlt} src={logo.imgSrc} />
                    </Link>
                  ) : (
                      <a href={logo.outterLink}>
                        <img alt={logo.imgAlt} src={logo.imgSrc} />
                      </a>
                    )}
                </Col>
              ) : null}
              <Col className="collapse-close" xs="6">
                <button className="navbar-toggler" type="button" onClick={toggleCollapse}>
                  <span />
                  <span />
                </button>
              </Col>
            </Row>
          </div>

          {/* Navigation */}
          <Nav navbar>
            {createLinks(routes)}
            {user ? (
              <NavLink
                key={69}
                onClick={logOutUser}
                to=""
                tag={NavLinkRRD}
                activeClassName="logout"
              >
                <i className="ni ni-circle-08 text-pink" />
                {t('AdminNavbar.dropdown_menu-logout')}
              </NavLink>
            ) : null}
          </Nav>
          {/* Divider */}
          <hr className="my-3" />
          {/* Heading */}
        </Collapse>
      </Container>
    </Navbar>
  );
}

Sidebar.defaultProps = {
  routes: [{}],
};

Sidebar.propTypes = {
  // links that will be displayed inside the component
  routes: PropTypes.arrayOf(PropTypes.object),
  logo: PropTypes.shape({
    // innerLink is for links that will direct the user within the app
    // it will be rendered as <Link to="...">...</Link> tag
    innerLink: PropTypes.string,
    // outterLink is for links that will direct the user outside the app
    // it will be rendered as simple <a href="...">...</a> tag
    outterLink: PropTypes.string,
    // the image src of the logo
    imgSrc: PropTypes.string.isRequired,
    // the alt for the img
    imgAlt: PropTypes.string.isRequired,
  }),
};

export default withTranslation('translations')(Sidebar);
