import React from "react";

import { withTranslation } from 'react-i18next';

import { FormGroup, Label, Input, Button } from "reactstrap";

import "../../assets/css/styles.css";

function MapForm({handleCoordinates, handleInputChange, handleSave, handleSearch, data, address, newHandleSave, ...props}) {

    const { t } = props;

    return (
    <div className="map-menu">
          <div className="from-input">
          <FormGroup>
                <Label for="fromRoute" className="d-none d-sm-block">
                  {t('SearchRouteForm.create-route-departure')}
                </Label>
                <Input
                  type="text"
                  name="from"
                  id="fromRoute"
                  placeholder={t(
                    'SearchRouteForm.create-route-placeholder-departure'
                  )}
                  onBlur={handleCoordinates}
                  onChange={handleInputChange}
                  value={address.from}
                  required
                />
              </FormGroup>
          </div>
          <div className="to-input">
          <FormGroup>
                <Label for="toRoute" className="d-none d-sm-block">
                  {t('SearchRouteForm.create-route-arrival')}
                </Label>
                <Input
                  type="text"
                  name="to"
                  id="toRoute"
                  placeholder={t('SearchRouteForm.create-route-placeholder-arrival')}
                  onBlur={handleCoordinates}
                  onChange={handleInputChange}
                  value={address.to}
                  required
                />
              </FormGroup>

          </div>
        <div className="input-buttons">
        
          <Button color="primary" className="search-button w-100 mt-sm-2" onClick={handleSearch}>
            {' '}
            <i className="fas fa-search-location mr-3"></i>{t('SearchRouteForm.similar-routes-btn')}
          </Button>
  
          <Button color="secondary" className="save-button w-100 mt-sm-3 ml-sm-0" onClick={newHandleSave}>
            {' '}
            <i className="fas fa-save mr-3"></i>{t('SearchRouteForm.save-routes-btn')}{' '}
          </Button>
     
        </div>

 </div>
    )
}

export default withTranslation('translations')(MapForm)