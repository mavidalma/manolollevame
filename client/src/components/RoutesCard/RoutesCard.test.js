import React from "react";
import { shallow, mount, render } from 'enzyme';
import RoutesCard from "./RoutesCard";



describe("Card with map", () => {

    const route = {
        _id: "5148asds8fsaf2fas4",
        title: "Ruta para testeo",
        city: "Madrid",
        from: {
            coordinates: [ -3.688451, 40.458866,  ]
        },
        to: {
            coordinates: [ -3.684644, 40.444293 ]
        }
    }

    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<RoutesCard {...route}/>);

    });
    
    test("should render", () => {

        expect(wrapper.exists()).toBe(true);
    });

    test("should render a Map", () => {

        expect(wrapper.find("Map")).not.toHaveLength(1);
    });

})
