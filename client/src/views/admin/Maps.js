import React, { useState } from "react";
import { Spinner } from 'reactstrap';
import T from "prop-types";
import config from '../../config';

import { GoogleMap, useLoadScript } from '@react-google-maps/api';

function MyMap({containerStyle, center, onClick, zoom, ...props}) {

  const { isLoaded, loadError } = useLoadScript({
  googleMapsApiKey: config.mapsApiKey,
  // ...otherOptions
})
  const renderMap = () => {
    // wrapping to a function is useful in case you want to access `window.google`
    // to eg. setup options or create latLng object, it won't be available otherwise
    // feel free to render directly if you don't need that
   // const onLoad = React.useCallback(
    //  function onLoad (mapInstance) {
        // do something with map Instance
    //  }
    //)
    return <GoogleMap
    mapContainerStyle={containerStyle}
    className="map-canvas"
    id="map-canvas"
    center={center}
    zoom={zoom || 10}
    onClick={onClick}
  >
  {props.children}
  </GoogleMap>
  }

  if (loadError) {
    return <div>Map cannot be loaded right now, sorry.</div>
  }

  return isLoaded ? renderMap() : <Spinner color="primary" />
}

MyMap.propTypes = {
  containerStyle: T.object.isRequired,
  center: T.object.isRequired,
  onClick: T.func,
  zoom: T.number,
};

export default MyMap
