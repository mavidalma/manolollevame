
import React, { useState, useEffect } from 'react';
import socketIOClient from "socket.io-client";

// reactstrap components
import {
  Card,
  CardBody,
  CardHeader,
  Row,
} from "reactstrap";
// core components

function Chat({ location, route }) {
  const [endpoint, setEndpoint] = useState(process.env.REACT_APP_API_URL);
  const [socket, setSocket] = useState('');
  const [userSelected, setUserSelected] = useState('');
  const [allMessages, setAllMessages] = useState([]);
  const [filterMessages, setFilterMessages] = useState('');
  const [allUsers, setAllUsers] = useState([]);
  const [msg, setMsg] = useState('');
  const [isOwner, setIsOwner] = useState('');
  const [userNewMessages, setUserNewMessages] = useState('');


  useEffect(() => {
    //conect to websocket
    const socket = socketIOClient(endpoint);
    setSocket(socket)


    // send user and route
    socket.emit("new connect", {
      username: localStorage.getItem('userName'),
      routeId: route
    });

    socket.on("messages", data => {

      setAllUsers(data.usersChat)

      setIsOwner(data.isOwner)

      if (data.isOwner) {
        setAllMessages(data.messages)
      } else {
        setFilterMessages(data.messages)
        //selet default route owner
        setUserSelected(data.usersChat[0])
      }
    });

    socket.on("message saved", async (data) => {

      if (data.isOwner) {
        newMsgOwner(data, socket.userSelected)
      } else {
        setFilterMessages(data.msgs)
      }
    });
    return () => {
      socket.emit("disconnectUser", {
        username: localStorage.getItem('userName')
      });
    }
  }, [])

  const newMsgOwner = (data, userSelectorSocket) => {
    //update users
    setAllUsers(data.usersChat)

    //get all messages
    setAllMessages(data.msgs)

    //filter msgs or not 
    if (data.userSelected === userSelectorSocket) {
      const userSelectedMsgs = data.msgs.filter(msg => {
        return msg.username === data.userSelected || msg.userReceiver === data.userSelected
      })
      setUserSelected(data.userSelected)
      setFilterMessages(userSelectedMsgs)
    } else {
      //user with new messages
      setUserNewMessages(data.userSelected)
    }
  }


  const selectUser = (e) => {
    const user = e

    if (isOwner) {
      const userSelectedMsgs = allMessages.filter(msg => {
        return msg.username === user || msg.userReceiver === user
      })
      setFilterMessages(userSelectedMsgs)
      socket.userSelected = user

      if (user === userNewMessages) {
        setUserSelected(user)
        setUserNewMessages('')

      } else {
        setUserSelected(user)
      }
    }
  }

  const sendMessage = (e) => {
    e.preventDefault();
    const data = {
      msg,
      userReceiver: userSelected
    }
    socket.emit('send message', data)
    setMsg('')
  }


  return (
    <>
      <Row>
        <div className={`col-3 pr-0 ${isOwner ? 'show' : 'd-none'}`}>
          <Card className="card-stats mb-4 mb-lg-0">
            <CardHeader className="p-2 text-center text-primary">
              Users
                </CardHeader>
            <CardBody className="p-0">
              <ul className="list-group">
                {!allUsers ? 'not users yet' :
                  allUsers.map((user, id) =>
                    <button
                      key={id}
                      value={user}
                      onClick={(e) => selectUser(e.target.value)}
                      className={
                        `list-group-item 
                        p-2 
                        rounded-0 
                          ${user === userNewMessages ? 'list-group-item-success' : 'list-group-item'}
                          ${user === userSelected ? 'active' : ''}
                          `}
                      type="button">
                      {user}
                    </button>
                  )}
              </ul>
            </CardBody>
          </Card>

        </div>
        <div className="col">
          <Card className="card-stats mb-4 mb-lg-0">
            <CardBody style={{
              height: "300px",
              overflow: "auto",
            }}>
              {!filterMessages ? '' :
                filterMessages.map(msg =>
                  <p key={msg._id}>
                    <b>{msg.username}:</b> {msg.msg}
                  </p>
                )}
            </CardBody>
          </Card>
          <form onSubmit={sendMessage} id="message-form" className="card-footer px-0 bg-white">
            <div className="input-group">
              <input
                value={msg}
                onChange={(e) => setMsg(e.target.value)}
                type="text"
                id="message"
                className="form-control"
              >
              </input>
              <button
                type="submit"
                disabled={userSelected ? false : true}
                className="btn btn-primary">
                Send
                  </button>
            </div>
          </form>
        </div>
      </Row>
    </>
  );
}


export default Chat;
