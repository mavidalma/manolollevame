
import React, { Component } from 'react';

class ErrorBoundary_Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      errorInfo: null
    };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    })
  }

  render() {
    if (this.state.errorInfo) {

      return (
        <>
          <div className="header bg-gradient-info pb-8 pt-5 pt-md-8">
            <h1 className="text-white text-center px-5">
              Ups..Something went wrong
              </h1>
          </div>
          <div className="mt--7 container-fluid ">
            <div className="row align-items-center justify-content-center">
              <div className="col-12 col-lg-6">
                <img alt="Register" className="my-4" style={{ "width": "90%" }} src={require("assets/img/theme/undraw_towing.svg")} />
              </div>
            </div>

            <h3 className="text-primary  text-center my-7">{this.state.error && this.state.error.toString()}</h3>
            {/* <details style={{ whiteSpace: "pre-wrap" }}>
                    {this.state.errorInfo.componentStack}
                  </details> */}
          </div>
        </>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary_Admin