
import Index from 'views/Index.js';
import Profile from 'views/profile/Profile.js';
import Register from 'views/auth/Register.js';
import Login from 'views/auth/Login.js';
import ChangePassword from 'views/auth/ChangePassword.js';
import ConfirmPassword from 'views/auth/ConfirmPassword.js';
import TermsConditions from 'views/admin/TermsConditions.js';
import RouteDetail from 'views/routes/RouteDetail.js';
import RouteCreate from 'views/routes/RouteCreate.js';
import RouteEdit from 'views/routes/RouteEdit.js';
import Chat from 'views/profile/Chat.js';


//invisible: true  to hidden

var routes = [
  {
    available: 'logged',
    path: '/index',
    name: 'Dashboard',
    icon: 'ni ni-tv-2 text-primary',
    component: Index,
    layout: '/admin',
  },
  {
    available: 'logged',
    path: '/user',
    name: 'Profile',
    icon: 'ni ni-circle-08 text-pink',
    component: Profile,
    layout: '/profile',
  },
  {
    path: '/create',
    available: 'logged',
    name: 'Route Create',
    invisible: true,
    component: RouteCreate,
    layout: '/route',
  },
  {
    path: '/edit/:id',
    available: 'logged',
    name: 'Route Edit',
    invisible: true,
    component: RouteEdit,
    layout: '/route',
  },
  {
    path: '/:id',
    available: 'any',
    name: 'Route detail',
    invisible: true,
    component: RouteDetail,
    layout: '/route',
  },
  {
    available: 'public',
    path: '/login',
    name: 'Login',
    icon: 'ni ni-key-25 text-info',
    component: Login,
    layout: '/auth',
  },
  {
    available: 'public',
    path: '/register',
    name: 'Register',
    icon: 'ni ni-circle-08 text-pink',
    component: Register,
    layout: '/auth',
  },
  {
    path: '/changepassword',
    name: 'ChangePassword',
    icon: 'ni ni-circle-08 text-pink',
    component: ChangePassword,
    layout: '/reset',
  },
  {
    path: '/confirmpassword',
    name: 'ConfirmPassword',
    icon: 'ni ni-circle-08 text-pink',
    component: ConfirmPassword,
    layout: '/reset',
  },
  {
    path: '/termsConditions',
    invisible: true,
    title: 'Terms and Conditions',
    name: 'Terms and Conditions',
    icon: 'ni ni-collection text-info',
    component: TermsConditions,
    layout: '/info',
  },
  {
    path: '/route',
    title: 'Chat',
    name: 'Chat',
    icon: 'ni ni-collection text-info',
    component: Chat,
    layout: '/chat',
  },
];
export default routes;
