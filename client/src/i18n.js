import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import XHR from 'i18next-xhr-backend';
import Cookies from 'js-cookie';

import translationEng from './locales/en/translation.json';
import translationSpa from './locales/es/translation.json';

const cookieLang = Cookies.get('manolo-locale') || 'en';

i18n
  .use(XHR)
  .use(LanguageDetector)
  .init({
    debug: true,
    lng: cookieLang,
    fallbackLng: 'en', // use en if detected lng is not available

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },

    resources: {
      en: {
        translations: translationEng,
      },
      es: {
        translations: translationSpa,
      },
    },
    // have a common namespace used around the full app
    ns: ['translations'],
    defaultNS: 'translations',
  });

export default i18n;
