'use strict';

const cote = require('cote');

const mailer = new cote.Requester({
  name: 'sendEmail',
  key: 'mail service'
});

const sendMail = (mail) => {
  return mailer.send({
    type: 'send email',
    from: mail.from,
    to: mail.to,
    subject: mail.subject,
    body: mail.body 
  });
};

module.exports = { sendMail };