'use strict';

const mongoose = require('mongoose');
const point = require('./pointSchema');

const routeSchema = new mongoose.Schema({
  user_id: {
    type: 'ObjectId',
    required: true,
    index: true,
  },
  city: {
    type: String,
    required: false, //[true, "City is required"]
  },
  title: {
    type: String,
    required: false, //[true, "Route name is required"]
  },
  description: {
    type: String,
  },
  transport: {
    type: String,
    required: false, //true,
  },
  seats: {
    type: Number,
    required: true,
  },
  departure: {
    type: String,
    required: true,
  },
  from: {type: point, required: true, index: '2dsphere'},
  to: {type: point, required: true, index: '2dsphere'}
});

routeSchema.statics.list = function (filter, limit, skip, sort) {
  const query = Route.find(filter);
  query.skip(skip).limit(limit);
  query.sort(sort);
  return query.exec();
};

routeSchema.statics.findWithin = async function (fromPolygon, toPolygon ) {

 const fromMatch = await Route.find().where('from').within(fromPolygon);
 const toMatch = await Route.find().where('to').within(toPolygon);

 const result = {fromMatch, toMatch};

  return result;
};

routeSchema.statics.matchPoints = async function (fromPoint, toPoint, radius = 1000) {
  //radius expressed in meters
  const fromMatch = await Route.find(( { from: { $geoWithin: { $centerSphere: [ fromPoint ,
    radius / 6378100 ] } } } ));
  
  const toMatch = await Route.find(( { to: { $geoWithin: { $centerSphere: [ toPoint ,
    radius / 6378100 ] } } } ));

  const result = {fromMatch, toMatch};

  return result;
  }

const Route = mongoose.model('Route', routeSchema);

module.exports = Route;
