'use strict';

const mongoose = require('mongoose');

//https://mongoosejs.com/docs/geojson.html

const pointSchema = new mongoose.Schema({
    type: {
      type: String,
      enum: ['Point'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  });

  module.exports = pointSchema;