const Route = require('../../models/Route');
const User = require('../../models/User');

//**** DATA ROUTE *******
module.exports = route = {
  data: async (idRoute) => {
    const route = await Route.findById(idRoute);
    const ObjectUserOwnerRoute = await User.findById(route.user_id)
    const userOwnerRoute = ObjectUserOwnerRoute.username;
    const titleRoute = route.title;

    return { userOwnerRoute, titleRoute };
  },


}




