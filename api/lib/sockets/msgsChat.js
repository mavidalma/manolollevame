const Chat = require('../../models/Chat');

//**** GET OLD MESSAGES *******
module.exports = msgs = {

  notOwner: (idRoute, username) => {
    const msgs = Chat.find({
      idRoute,
      $or: [
        { username: username },
        { userReceiver: username },
      ]
    }).sort({ created_at: 1 });
    return msgs
  },

  owner: (idRoute) => {
    const msgs = Chat.find({
      idRoute: idRoute
    }).sort({ created_at: 1 });
    return msgs
  },

  sendMsg: async (username, userOwnerRoute, routeId, msg, userReceiver) => {
    const newMsg = new Chat({
      username: username,
      userOwnerRoute: userOwnerRoute,
      idRoute: routeId,
      msg: msg,
      userReceiver: userReceiver
    })

    await newMsg.save();
  }
}




