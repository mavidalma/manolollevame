'use strict';

const express = require('express');
const router = express.Router();
const jwtAuth = require('../lib/jwtAuth');

const Route = require('../models/Route');

router.get('/', async (req, res, next) => {
  /* const LIMIT = 3;
  const SKIP = 0; */

  try {
    const user = req.query.userId;
    const limit = parseInt(req.query.limit);
    const sort = req.query.sort;
    const skip = parseInt(req.query.skip);

    const filter = {};

    if (typeof user !== 'undefined') {
      filter.user_id = user;
    }

    const docs = await Route.list(filter, limit, skip, sort);
    //console.log(docs)
    res.json(docs);
  } catch (err) {
    next(err);
  }
});

//create a new Route
// POST /routes/
router.post('/create', async (req, res, next) => {
  try {
    const route = req.body;
    let newRoute = new Route(route);
    let user_id = req.body.user_id;
    // console.log("route recieved: ", route, "user ID: ", user_id, "route created: ", newRoute)
    /*  if(user_id === "anon") {user_id = mongoose.mongo.BSONPure.ObjectID.fromHexString(user_id) ;
  }

    console.log("changed ID: ", user_id)
    newRoute = {...newRoute, user_id}
    console.log("new new route, after spread: ", newRoute)*/
    const createRoute = await newRoute.save();

    // console.log("createRoute: ", createRoute)

    res.send({ success: true, savedRoute: createRoute });
  } catch (err) {
    next(err);
  }
});

//find routes within given polygons
//POST /routes/find
router.post('/find', async (req, res, next) => {
  try {
    const from = req.body.from;
    const to = req.body.to;
    console.log(from, to);

    const matchings = await Route.findWithin(from, to);
    if (!matchings.fromMatch && !matchings.toMatch) {
      const error = new Error('there are not any matching routes');
      error.status = 401;
      res.status(401).json({
        success: false,
        error: error.message,
        description: 'there are no matching routes. ',
      });
      return;
    }

    //it will send any route with EITHER a "from" match or a "to" match. Or both, obviously.

    res.send({ success: true, matchings });
  } catch (err) {
    next(err);
  }
});

//find routes matching a given radius from departure point "from"
// AND/OR arrival point "to"
// SCHEDULE MATCHING ??
// POST /routes/match

router.post('/match', async (req, res, next) => {
  try { 
  const from = req.body.from.coordinates;
  const to = req.body.to.coordinates;

  // const radius = req.maxDistance  ---> NOT IMPLEMENTED YET ON FRONT

  const match = await Route.matchPoints(from, to);

  res.send({success: true, results: match});
  } catch (err) {
    next(err)
  }
})

// Find a route by its ID
// GET /routes/:id

router.get('/:id', async (req, res, next) => {
  try {
    const _id = req.params.id;
    const route = await Route.findOne({ _id });

    if (!route) {
      const err = new Error('not found');
      err.status = 404;
      next(err);
      return;
    }

    res.json({ result: route });
  } catch (err) {
    next(err);
  }
});

/**
 * Update a route
 * PUT /routes/:id
 */

router.put('/:id', async (req, res, next) => {
  try {
    const _id = req.params.id;
    const routeData = req.body;

    const saveRoute = await Route.findOneAndUpdate({ _id }, routeData, {
      new: true,
      useFindAndModify: false,
    });

    res.json({ result: saveRoute });
  } catch (err) {
    next(err);
  }
});

/**
 * Delete a route
 * DELETE /routes/:id
 */

router.delete('/:id', async (req, res, next) => {
  try {
    const _id = req.params.id;
    await Route.deleteOne({ _id });

    res.json();
  } catch (err) {
    next(err);
  }
});

module.exports = router;
