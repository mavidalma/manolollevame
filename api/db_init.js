'use strict';

require('dotenv').config();

const conn = require('./lib/mongooseConnect');
const initRoutes = require('./lib/initRoutes.json');
const Route = require('./models/Route');
const User = require('./models/User');

conn.once('open', async () => {
  try {
    await initUsers();
    await initRoute();
    conn.close();

  } catch (err) {
    console.error('there has been an error reinitializing the DB: ', err);
    process.exit(1);
  }
});

const initRoute = async () => {
  await Route.deleteMany();
  await Route.insertMany(initRoutes);
};

const initUsers = async () => {
  await User.deleteMany();
  await User.insertMany([
    {
      '_id': '5e6e8e074763f31a1ad53674',
      'username': 'Manolo',
      'name': 'Manuel',
      'surname': 'Perez',
      'city': 'Lugo',
      'country': 'España',
      'email': 'manolo@llevame.com',
      'password': await User.hashPassword('vivaManolo'),
      'routes': ['5e6bf231992d1b176ff18e33', '7e6959c1924af424589d014d'],
      'chatOpens': [],
      'likes': 3
    },
    {
      '_id': 'Ee6e8e074763f31a1ad53675',
      'username': 'copiloto',
      'name': 'Jane',
      'surname': 'Doerz',
      'city': 'Madrid',
      'country': 'España',
      'email': 'fake@mail.com',
      'password': await User.hashPassword('llevame'),
      'routes': ['5e6959c1b244f42c589d014d', '5e6959c19244f424589d014d', '5e6959c1b244f42c599d014d'],
      'chatOpens': [],
      'likes': 1
    }
  ]);
};